# EventType: 11502020
#
# Descriptor: {[[B0]nos -> (tau+ -> pi+ pi- pi+ anti-nu_tau) (tau- -> pi+ pi- pi- nu_tau) (K*(892)0 -> K+ pi-)]cc, [[B0]os -> (tau- -> pi+ pi- pi- nu_tau)(tau+ -> pi+ pi- pi+ anti-nu_tau) (K*(892)~0 -> K- pi+)]cc}
#
# NickName: Bd_Ksttautau,3pi3pi=DecProdCut,TightCut2,tauolababar
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#gen = Generation()
#gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay = "[ ^(B0 ==>  ^(K*(892)0 -> ^K+ ^pi-) ^(tau+ ==> ^pi+ ^pi- ^pi+ nu_tau~) ^(tau- ==> ^pi- ^pi+ ^pi- nu_tau) ) ]CC"
#tightCut.Preambulo += [
#  "from LoKiCore.functions import in_range"  ,
#  "from GaudiKernel.SystemOfUnits import GeV, MeV"  
#]
#tightCut.Cuts = {
# '[B0]cc'       : " ( GPT > 8 * GeV ) & ( GP > 60 * GeV ) " ,
# '[K*(892)0]cc' : " ( GPT > 1 * GeV ) & ( GP > 10 * GeV ) " ,
# '[tau+]cc'     : " ( GPT > 2 * GeV ) & ( GP > 20 * GeV ) " ,
# '[K+]cc'       : " in_range( 0.010 , GTHETA , 0.400 ) & ( GPT > 250 * MeV ) & ( GP > 4 * GeV ) " ,
# '[pi-]cc'      : " in_range( 0.010 , GTHETA , 0.400 ) & ( GPT > 250 * MeV ) & ( GP > 2 * GeV ) "
# }
# EndInsertPythonCode

# Documentation: Bd decay to K* tau tau.
# Both tau leptons decay in the 3-prong charged pion mode using the Tauola BaBar model.
# All final-state products in the acceptance.
# Same as 11508000, but with different tau decay model and additional cuts on P and PT.
# EndDocumentation
#
# PhysicsWG: RD
#
# CPUTime: 9 min
# Tested: Yes
# Responsible: Andrey Tayduganov
# Email: tayduganov@cppm.in2p3.fr
# Date: 20170515
#

# Tauola steering options
Define TauolaCurrentOption 1
Define TauolaBR1 1.0
#
Alias         Mytau+     tau+
Alias         Mytau-     tau-
ChargeConj    Mytau+     Mytau-
Alias         MyK*0      K*0
Alias         Myanti-K*0 anti-K*0
ChargeConj    MyK*0      Myanti-K*0
#
Decay B0sig
  1.000       MyK*0      Mytau+    Mytau-       BTOSLLBALL;
Enddecay
CDecay anti-B0sig
#
Decay MyK*0
  1.000       K+         pi-       VSS;
Enddecay
CDecay Myanti-K*0
#
Decay Mytau-
  1.00        TAUOLA 5;
Enddecay
CDecay Mytau+
#
#
End

