# EventType: 12113445
#
# Descriptor: [B+ -> FLATQ2 (K*+ -> K+ pi0) mu+ mu-]cc
#
# NickName: Bu_Kstmumu,Kpi0=PHSP,flatq2,DecProdCut,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: Decay file for B+ -> (K*+ -> K+ pi0) mu+ mu- (PHSP, flat Q2 and pi0PTcut)
# EndDocumentation
#

# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '^[B+ -> ^(K*(892)+ -> ^K+ ^(pi0 => ^gamma ^gamma)) ^mu+ ^mu-]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer,MeV,GeV',
#     'inAcc     =  in_range( 0.005 , GTHETA , 0.400 )',
#     'goodPi0  = (GPT > 630 * MeV) & inAcc'
# ]  
# tightCut.Cuts     =    {
#     '[pi0]cc'          : 'goodPi0',
#     '[mu+]cc'          : 'inAcc',
#     '[pi+]cc'          : 'inAcc'
#     }
# EndInsertPythonCode
#
#
# CPUTime: < 1min
#
# PhysicsWG:   RD
# Tested:      Yes
# Responsible: David Gerick
# Email:       david.gerick@cern.ch
# Date:        20170424
#
# $Id$
#
Alias MyKst+ K*+
Alias MyKst- K*-
ChargeConj MyKst+ MyKst-
#
Decay B+sig
 1.0     MyKst+ mu+ mu- FLATQ2 1;
Enddecay
CDecay B-sig
#
Decay MyKst+
 1.0 K+ pi0 PHSP;
Enddecay
CDecay MyKst-
#
End
#
