# EventType: 12165701
#
# Descriptor: [B+ ->  (anti-D*0 -> (anti-D0 -> ((K_S0 -> pi+ pi-) pi+ pi- (pi0 -> gamma gamma)))  (pi0 -> gamma gamma)) pi+]cc
#
# NickName: Bu_Dst0Pi,D0pi0,Kspipipi0=TightCut,PHSP
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '^[B+ -> ^(D*(2007)~0 -> ^(D~0 => ^(KS0 ==> ^pi+ ^pi-) ^pi+ ^pi- ^(pi0 -> ^gamma ^gamma)) (pi0 -> gamma gamma)) ^pi+]CC'
# tightCut.Preambulo += [
#     'GVZ = LoKi.GenVertices.PositionZ() ' ,
#     'from GaudiKernel.SystemOfUnits import millimeter',
#     'inAcc        = (in_range (0.005, GTHETA, 0.400))',
#     'inEcalX   = abs ( GPX / GPZ ) < 4.5 / 12.5      ' ,
#     'inEcalY   = abs ( GPY / GPZ ) < 3.5 / 12.5      ' ,
#     'goodB        = (GP > 7500 * MeV) & (GPT > 1500 * MeV) & (GTIME > 0.105 * millimeter)',
#     'goodD        = (GP > 4000 * MeV) & (GPT > 400 * MeV)',
#     'goodKS       = (GNINTREE(("KS0"==GABSID) & (GP >  4000 * MeV) & (GPT >  400 * MeV) & (GFAEVX(abs(GVZ), 0) < 2500.0 * millimeter))>0.5)',
#     'goodDaugPi  = (GNINTREE (("pi+" == GABSID) & (GP > 750 * MeV) & inAcc, 1) > 1.5)',
#     'goodKsDaugPi = (GNINTREE (("pi+" == GABSID) & (GP > 500 * MeV) & inAcc, 1) > 1.5)',
#     'goodBachPi   = (GNINTREE (("pi+" == GABSID) & (GP > 5000 * MeV) & (GPT > 500 * MeV) & inAcc, 1) > 0.5)',
#     'goodPi0      =  (GNINTREE( ("pi0"==GABSID) & (GP > 750 * MeV) & (GPT > 300 * MeV) & inAcc, 1) > 0.5)',
#     'goodPi0Gamma = (GNINTREE( ("gamma"==GABSID) & (GP > 750 * MeV) & (GPT > 400 * MeV) & inEcalX  & inEcalY  & inAcc, 1) > 1.5)',
# ]
# tightCut.Cuts      =    {
#     '[B+]cc'         : 'goodB  & goodBachPi',
#     '[D0]cc'         : 'goodD & goodDaugPi & goodPi0 & goodKS' ,
#     '[KS0]cc'        : 'goodKsDaugPi',
#     '[pi0]cc'        : 'goodPi0Gamma',
#     }
# EndInsertPythonCode
#
# Documentation: B decays to Dstar0pi, Dstar0 decays to D0 pi0, D0 decays to KSpipipi0  PHSP, KS decays to pi+pi-, pi0 decays to gamma gamma 
# all decay products , and including gammas, in acceptance and tight cuts
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: 2 min
# Responsible: Vincent Tisserand
# Email: tisserav@lapp.in2p3.fr
# Date: 20170425
#
Alias myD*0       D*0
Alias myanti-D*0  anti-D*0
ChargeConj myD*0  myanti-D*0
Alias myD0 D0
Alias myanti-D0 anti-D0
ChargeConj myD0 myanti-D0
Alias myK_S0 K_S0
ChargeConj myK_S0 myK_S0
Alias myPi0 pi0
ChargeConj myPi0  myPi0
##
Decay B+sig
  1.000     myanti-D*0  pi+               SVS ;
Enddecay
CDecay B-sig
#
Decay myanti-D*0
1.0000    myanti-D0  myPi0         VSS;
Enddecay
CDecay myD*0
#
Decay myD0
1.000     myK_S0 pi+  pi- myPi0  PHSP;
Enddecay
CDecay myanti-D0
#
Decay myK_S0
1.000     pi+  pi-  PHSP;
Enddecay
#
Decay myPi0
1.0000  gamma gamma    PHSP;
Enddecay
#
End
